// 预编译器，把stdio.h文件中的所有内容都输入该行所在的位置, stdio.h 包含了供编译器使用的输入输出函数
#include <stdio.h> // <- C 预处理指令
#include <MacTypes.h>
#include "limits.h"

#define PSQUARE(X) printf(""#X"的平方结果是: %d\n", ((X)*(X)))

#define INDEXX(N) x ## N
#define PRINT_INDEXX(N) printf("x"#N"=%d\n", x ## N );

char * itobs(int n, char * ps){
    int i;
    const static int size = CHAR_BIT * sizeof(int);
    for (i = size - 1; i >= 0; i--, n >>= 1) {
        ps[i] = (01 & n) + '0';
    }
    ps[size] = '\0';
    return ps;
}

void testFile();

void fileShow();

void test2();

void 取反();

void 按位与();

void testItobs();

void oneSharp();

void twoSharp();

struct man{
    char name[40];
    int age;
};

int main() { // <- 函数体开始

//    twoSharp();

//    oneSharp();

//    testItobs();


//    按位与();

//    取反();

//    test2();


    //    fileShow();


    return 0; // <- 函数体结束
}

void twoSharp() {
    int INDEXX(1) = 12; //int x1 = 12
    int INDEXX(2) = 13; //int x2 = 13
    int INDEXX(3) = 2; //int x3 = 2
    PRINT_INDEXX(1); // x1=12
    PRINT_INDEXX(2); // x2=13
    PRINT_INDEXX(3); // x3=2

}

void oneSharp() {
    PSQUARE(2);
    PSQUARE((2 + 3));
    PSQUARE(2 + 3);
    int y = 6;
    PSQUARE(y);
}

void testItobs() {
    printf("sizeof(int) = %ld\n", sizeof(int));
    char bit_str[CHAR_BIT * sizeof(int) + 1];
    int number = -11;
    itobs(number, bit_str);
    printf("%d 的二进制是：\n", number);
    for (int i = 0; bit_str[i]; ) {
        putchar(bit_str[i++]);
    }
}

void 按位与() {
    unsigned char a = 2;   // 00000010
    unsigned char b = 1;    // 00000001
    printf("%d", a & b);    // 00000000

}

void 取反() {
    char a = 2;  // 00000010
    printf("%d", ~a);   // 11111101 第一个是符号位，计算机判断负数，取补码 10000011 即 -3

}

void test2() {
    struct man afra = {
            "Afra55",
            2
    };

    printf("我的名字是%s, 我今年%d岁了", afra.name, afra.age);


    struct man bfra55 = {.age = 12, .name = "Bfra55"};
}

void fileShow() {//    testFile();


    long last;
    FILE * fp = fopen("../test/test.txt", "rb");
    // 定位到文件末尾
    fseek(fp, 0, SEEK_END);
    int ch;
    last = ftell(fp);
    for (long i = 1; i <= last; ++i) {
        // 定位到文件末尾的前 i 位
        fseek(fp, -i, SEEK_END);
        ch = getc(fp);
        putchar(ch);
    }
    fclose(fp);
}

void testFile() {
    int ch;
    FILE * fp = fopen("../test/test.txt", "r");
    FILE * fpout = fopen("../test/testout.txt", "w");
    ch = getc(fp);
    while ((ch = getc(fp)) != EOF) { // 得到一个输入,判断是否是文件末尾
        putchar(ch);
        putc(ch, fpout);
    }

    if(fclose(fp) != 0){

        printf("关闭test文件失败\n");
    }
    if(fclose(fpout) != 0){
        printf("关闭testout文件失败\n");
    }
}
