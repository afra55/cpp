//
// Created by Victor on 2019/10/22.
//

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include "myrand.c"
#define MSG "define a message"
#define MAX_LENGTH 81

extern unsigned int myrand(void);

void test1();

void test2();

void test3();

void test4();

void text5();

void test6();

void scanfTest();

void putsTest();

void fputsTest();

void strlenTest();

void stringTest2();

void cmp();

void cmp2();

void cmp3();

void cpy();

void sprint();

void toUpperChar();

int punchCount();

void upper();

void punch();

int main() {

//    test1();

//    test2();
//    test3();

//    test4();

//    text5();

//    test6();

//    putsTest();

    //    scanfTest();

//    fputsTest();

//    strlenTest();


//    stringTest2();

//    cmp();

//    cmp2();

//    cmp3();

//    cpy();

//    sprint();

//    upper();

//    punch();

//    printf("%d\n", atoi("333"));
//    printf("%d\n", atoi("333abcde"));
//    printf("%d\n", atoi("abced333"));
//
//
//    char a[] = "1234defg";
//    char * end;
//    long value = strtol(a, &end, 10);
//    printf("%ld, %s, %d", value, end, *end);

    for (int i = 0; i < 10; ++i) {
        printf("%d\n", myrand());
    }

    return 0;
}

void punch() {
    char a[] = "a, bc, c, c!@#$";
    printf("a 的字符有 %d 个", punchCount(a));
}

int punchCount(char * a){
    int count = 0;
    while (*a){
        if (ispunct(*a)) {
            count++;
        }
        a++;
    }
    return count;
}

void upper() {
    char a[] ="abcdef";
    toUpperChar(a);
    puts(a);
}

void toUpperChar(char * a){
    while (*a){
        *a = toupper(*a);
        a++;
    }
}

void sprint() {
    char a[20];
    sprintf(a, "%s %d $%6.2f", "hahah", 12, 987.123);
    puts(a);
}

void cpy() {
    char temp[8];
    char a[] = "afra55";
    strcpy(temp, a);
    puts(a);
    puts(temp);
}

void cmp3() {
    const char * list[3] = {"victor blallalal", "bbbbvictorcccc", "ssssss victor"};
    printf("查找 list[0]: %d\n", strncmp(list[0], "victor", 6));
    printf("查找 list[1]: %d\n", strncmp(list[1], "victor", 6));
    printf("查找 list[2]: %d\n", strncmp(list[2], "victor", 6));
}

void cmp2() {
    printf("A 与 A 比较: %d\n", strcmp("A", "A"));
    printf("A 与 B 比较: %d\n", strcmp("A", "B"));
    printf("A 与 C 比较: %d\n", strcmp("A", "C"));
    printf("C 与 A 比较: %d\n", strcmp("C", "A"));
    printf("D 与 A 比较: %d\n", strcmp("D", "A"));
    printf("DDDDDD 与 AAAAAA 比较: %d\n", strcmp("DDDDDD", "AAAAAA"));
}

void cmp() {
    char a[] = "I miss u";
    char b[] = "I miss u";
    char c[] = "U miss i";
    printf("a 与 b 比较: %d\n", strcmp(a, b));
    printf("a 与 c 比较: %d", strcmp(a, c));
}

void stringTest2() {
    char a[10] = "I u.";
    char b[] = "you";
    strncat(a, b, 2);
    puts(a);
    puts(b);
}

void strlenTest() {
    char a[] = "I Love U\0Hi hahahahah";
    puts(a);
    printf("%lu\n", strlen(a));
    puts(a + 9);
}

void fputsTest() {
    char str[80] = "Hi i am Afra55 from victor";
    char str2[80] = "I come from your dream!";
    puts(str);
    puts(str2);
    fputs(str2, stdout);
    fputs("is fputs", stdout);
}

void putsTest() {
    char think[5];
    int i;
    puts("请输入你的想法");
    // 重复获取输入内容，直到文件末尾或该行第一个字符是换行符时结束
    while (fgets(think, 5, stdin) != NULL && think[0] != '\n'){
        i = 0;
        // 找到该行的结尾，如果是换行符则该行内容没有超出规定大小
        while (think[i] != '\n' && think[i] != '\0')
            i++;
        if (think[i] == '\n'){
            think[i] = '\0';
        } else{
            // 这个分支是该行内容多与规定大小，为了舍弃多余字符，让剩余的字符读取但不存储, 包括遇到但第一个换行符
            while (getchar() != '\n'){}
        }
        puts(think);
    }
}

void test6() {
    char think[5];
    puts("请输入你的想法");
    fgets(think, 5, stdin); // 第二个参数表示读入字符的最大长度数量是 4个，第三个参数是读入第文件 stdin 是标准输入
    puts(think);
    fputs(think, stdout); // stdout 标准输出

}

void text5() {
    char think[3];
    puts("请输入你的想法");
    gets(think);
    printf("已经接受到你到想法: %s\n", think);
    puts(think);
}

void test4() {
    const char * a = "I Love U";
    const char * b;

    b = a;
    printf("%s\n", b);
    printf("a = %s, &a = %p, a指针变量存储的地址值 = %p\n", a, &a, a);
    printf("b = %s, &b = %p, b指针变量存储的地址值 = %p\n", b, &b, b);
}

void test3() {// 字符串存储在静态存储区
// 在程序运行的时候才给 a 数组分配内存，之后才将字符串'拷贝'到数组中
    char a[] = MSG;

    // 字符串字面量是常量 const 数据
// 由于 pt 指向const数据，所以 pt 应该声明为 const 数据到指针
    const char * pt = MSG;
    printf("%p\n", "define a message");  // 0x103f1aeb0
    printf("%p\n", a);  // 0x7ffeebce5af0
    printf("%p\n", pt);   // 0x103f1aeb0
    printf("%p\n", MSG);   // 0x103f1aeb0
    printf("%p\n", "define a message");   // 0x103f1aeb0


}

void scanfTest() {
    int count;
    char think1[10], think2[10];
    printf("请输入你的想法:\n");
    count = scanf("%5s %3s", think1, think2);
    printf("%d %s %s", count, think1, think2);
}

void test2() {
    char hi[MAX_LENGTH] = "Hi, I miss u";
    printf("%p\n", hi);     // 0x7ffee2023ab0
    printf("%p\n", &hi[0]);     // 0x7ffee2023ab0
    printf("%c\n",*hi);     // H
    printf("%c\n", hi[0]);      // H
    printf("%c\n", *(hi + 1));      // i
    printf("%c\n", hi[1]);      // i
}

void test1() {
    char hi[MAX_LENGTH] = "Hi, i am victor yang!";
    const char * pt = "i do not know, why you dismiss.";
    puts("your message:"); // puts 函数只显示字符串，并且自动在末尾加换行符
    puts(MSG);
    puts(hi);
    puts(pt);
    hi[3] = 'H';
    puts(hi);
}
