//
// Created by Victor on 2019/10/22.
//

#include <stdio.h>
#include <math.h>
void antCode();
void nineNumber();
void nineNumberLogic(int a, int n);
int main() {
    int x = 73;
    printf("十进制 = %d，八进制 = %o，十六进制 = %x\n", x, x, x);
    printf("十进制 = %d，八进制带前缀 = %#o，十六进制带前缀 = %#x\n", x, x, x);
//    antCode();
//    nineNumber();

    unsigned short us = -1;
    printf("%d\n", us);

    printf("65: %c\n", 65);
    printf("A: %c\n\n", 'A');

    printf("int 类型有 %d 字节\n", sizeof(int));
    printf("char 类型有 %d 字节\n", sizeof(char));
    printf("long 类型有 %d 字节\n", sizeof(long));
    printf("long long 类型有 %d 字节\n", sizeof(long long));
    printf("double 类型有 %d 字节\n", sizeof(double));
    printf("long double 类型有 %d 字节\n", sizeof(long double));
    printf("_Bool 类型有 %d 字节\n", sizeof(_Bool));
    printf("float 类型有 %d 字节\n", sizeof(float));

    return 0;
}

/**
 * 有一根27厘米的细木杆，
 * 在第3厘米、7厘米、11厘米、17厘米、23厘米这五个位置上各有一只蚂蚁。
 * 木杆很细，不能同时通过两只蚂蚁。开始时，蚂蚁的头朝左还是朝右是任意的，
 * 它们只会朝前走或调头，但不会后退。当任意两只蚂蚁碰头时，两只蚂蚁会同时调头朝反方向走。
 * 假设蚂蚁们每秒钟可以走一厘米的距离。编写程序，求所有蚂蚁都离开木杆的最短时间和最长时间。
 */
void antCode() {
    int antSize = 5;
    double nums[] = {3, 7, 11, 17, 23};
    printf("sizeof(nums)=%d\n", antSize);
    double lineSize = 27;

    double max = 0;
    double min = nums[0];
    for (int i = 0; i < antSize; ++i) {
        double left = nums[i];
        double right = lineSize - nums[i];
        printf("%d: left=%f, right=%f\n", i, left, right);
        max = fmax(max, fmax(left, right));
        min = fmax(min, fmin(left, right));
    }

    printf("max=%f; min = %f\n\n\n\n\n", max, min);
    
}

/**
 * 找出符合如下条件的 9位数：
 * 这个数包括了 1 ～ 9这 9个数字。这个 9位数的前 n位都能被 n整除，若这个数表示为 abcdefghi，则 ab可以被 2整除， abc可以被 3整除…… abcdefghi可以被 9整除。
 */
void nineNumber(){

    for (int i = 1; i < 10; ++i) {
        nineNumberLogic(i, 2);
    }

}

void nineNumberLogic(int a, int n){
    if (n > 9) {
        printf("%d\n", a);
        return;
    }
    int first = a * 10;
    // 这里可以判断奇偶数来进一步提高效率
    for (int i = 1; i < 10; ++i) {
        int b = first + i;
        if (b % n == 0) {
            nineNumberLogic(b, n + 1);
        }
    }
}