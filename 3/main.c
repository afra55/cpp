//
// Created by Victor on 2019/10/22.
//

#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <ctype.h>

#define HAPPY "欢迎你的光临"
#define PI = 3.1415926

int main() {

    char name[40];
    printf("你叫什么名字呢?\n");
    scanf("%s", name);
    printf("你好, %s, %s\n", name, HAPPY);

    printf("name 存储了多少个字符? %zd\n", strlen(name));

    const int YEAR_DAY = 365;

    printf("循环开始---\n");
    int a = 0;
    while (a < 10) {
        a++;
        if (a % 2 == 0) {
            continue;
        }
        printf("%d ", a);
        if (a > 8) {
            break;
        }
    }
    printf("\n循环结束--\n");

    for (int i = 0; i < 10; ++i) {
        printf("%d", i);
    }

    int i = 0;
    do {
        ++i;
    } while (i < 10);

    int i1 = 101;
    if(i1 > 100){
        printf("大于100 %d", i1);
    } else if (i1 > 50) {
        printf("大于50 %d", i1);
    } else {
        printf("小于 50");
    }

    int i2 = 2;
    int i3 = i2 > 5 ? 1 : -1;
    printf("%d", i3);

    int i4 = 2;
    switch(i4){
        case 1:
            printf("我是1");
            break;
        case 2:
            printf("我是1");
            break;
        default:
            printf("我是谁");
            break;
    }
    return 0;
}

void a(int a);

void b(){
    a(10);
}

void a(int a){
    printf("%d", a);
}